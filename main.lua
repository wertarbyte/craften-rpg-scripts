--Test area
local particle = require 'rpgplus.particle'

local scheduler = require 'rpgplus.scheduler'

local spawnJunkTrader = require 'mobs/traders/junkTrader'
spawnJunkTrader("rpg", 39, 32, 565)

local woodcutter = require 'mobs/traders/woodcutter'
woodcutter("rpg", 39, 35, 565)

local rpgplus = require 'rpgplus'

local rat = require 'mobs/enemies/rat'
rat.spawn("rpg", {{x=27, y=32, z=547},{x=29.929, y=32, z=547},{x=32.955, y=32, z=547}})

local wolf = require 'mobs/enemies/wolf'
wolf.spawn({}, {{world="rpg",x=27, y=32, z=550},{world="rpg",x=29.929, y=32, z=550},{world="rpg",x=32.955, y=32, z=550}})

local playerUtil = require 'util/player'

local inventory = require 'rpgplus.inventory'
local itemstack = require 'items/itemstack'
local chatcolor = require 'util/chatcolor'

rpgplus.on("player.interact", function(event)
	if event:hasItem() then
		if chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(event:getItem()), "§"), "&") == "Charakter Informationen" then
			event:setCancelled(true);
			local player = event:getPlayer()
			local level = playerUtil.getLevel(player)
			local xp = playerUtil.getXp(player)
			local totalXp = playerUtil.getTotalXp(player)
			local xpForLevel = playerUtil.getExpAmountForLvl(level)
			local items = {}
			
			items[4] = {type="exp_bottle", amount=1, name="&aLevel Informationen", lore={"&aLevel: " .. level, "&aFortschritt: " .. xp .. "/" .. xpForLevel .. " (" .. (math.floor(xp/xpForLevel*100)) .. "%)"}}
			items[10] = {type="enchanted_book", amount=1, name="&5Skills", lore={"&7Clicke um das Skill-Inventar zu öffnen"}}
			
			local inv = inventory.openChest(player, 3, "&bCharakter Informationen", items)
			inv:on("click", function(event) 
				event:setCancelled(true)
				
				if chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(event:getCurrentItem()), "§"), "&") == "Skills" then
					
					playerUtil.openSkillInv(player, inv)
					
				end
				
			end)
		end
	end
end)

--Test area

require 'handler/joinhandler'
require 'handler/attackhandler'
require 'handler/regainhandler'
require 'handler/damagehandler'
require 'handler/protectionhandler'
require 'handler/noweather'
require 'handler/spawnhandler'

require 'commands/getitem'
require 'commands/resetstats'
require 'commands/playsong'
require 'commands/getkeyv'
require 'commands/setkeyv'
require 'commands/getmoney'

require 'commands/opwhitelist'

require 'mobs/traders/traders'
require 'mobs/villager/villagers'

require 'mobs/enemies/wolfs'

require 'quests/quests'

require 'spells/spell'.init()

local staticitems = require 'handler/staticitems'
staticitems.newStaticItem("Charakter Informationen")

luajava.bindClass("org.bukkit.Bukkit"):getServer():clearRecipes()

rpgplus.on("entity.death", function(event)
	event:setDroppedExp(0)
	event:getDrops():clear()
end)
