local rpgplus = require 'rpgplus'
local money = require 'util/money'

local command = "&cSyntax: /rpgmoney 'player' 'get:set:give:take' ('amount')"

rpgplus.playercommand("rpgmoney", function(sender, cmd, player, variable, amount)
	if sender:hasPermission("rpgplus.command.rpgmoney") then
		if player ~= nil and variable ~= nil then
			if variable=="give" then
				money.give(player, amount)
				rpgplus.sendMessage(sender, "Player has "..money.get(player).." Money.")
			elseif variable=="take" then
				money.take(player, amount)
				rpgplus.sendMessage(sender, "Player has "..money.get(player).." Money.")
			elseif variable=="get" then
				rpgplus.sendMessage(sender, "Player has "..money.get(player).." Money.")
			elseif variable=="set" then
				money.set(player, amount)
				rpgplus.sendMessage(sender, "Player has "..money.get(player).." Money.")
			end
		else
			rpgplus.sendMessage(sender, command)
		end
	end
end)