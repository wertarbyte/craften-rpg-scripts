local rpgplus = require 'rpgplus'

local oplist = {
   '79df38f9-a157-4177-b1df-e4d025576d6f', -- FlorestanII
   '94d67f2f-d039-419b-8958-abe6b25916b0', -- leMaik
   'a2a3af71-08c7-4f3c-b511-0afe60b851fc', -- MarkusK96
   'a80317ee-89fc-4529-bdef-b6a3c0e631b4', -- Jan_Gootha
   'e8336053-d887-4f3f-8e58-89e4a6868323', -- samy_play_1997
   'dcc4b6e7-6c94-41a1-8d94-490423d848f1'  -- LordSim0n
}

function string.starts(text, start)
   return string.sub(text, 1, string.len(start))==start
end

local function has_value (tab, val)
    for index, value in ipairs (tab) do
        if value == val then
            return true
        end
    end

    return false
end

local function split(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; local i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end

rpgplus.on("player.command.preprocess", function(event)
	if string.starts(event:getMessage(), "/op") then
		local player = event:getPlayer():getUniqueId():toString()
		
		if not has_value(oplist, player) then
			event:setCancelled(true)
		end
	end
end)
