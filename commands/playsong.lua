local rpgplus = require 'rpgplus'
local song = require 'util/song'

local command = "&cSyntax: /playsong ('player') 'songname'"

rpgplus.playercommand("playsong", function(sender, cmd, player, songname)
	if sender:hasPermission("rpgplus.command.playsong") then
		if player ~= nil and songname ~= nil then
			song(player, songname)
		elseif player ~=nil and songname == nil then
			song(sender, player)
		else
			rpgplus.sendMessage(sender, command)
		end
	end
end)