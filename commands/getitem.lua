local rpgplus = require 'rpgplus'
local inventory = require 'rpgplus.inventory'

local weapons = require 'items/weapons/weapons'
local armor = require 'items/armor/armor'

rpgplus.playercommand("getitem", function(sender, cmd, args)
	if sender:hasPermission("rpgplus.command.getitem") then
		if weapons.isWeapon(args) then
			inventory.giveItems(sender, weapons.getWeapon(args).get())
		end
		if armor.isArmor(args) then
			inventory.giveItems(sender, armor.getArmor(args).get())
		end
	end
end)