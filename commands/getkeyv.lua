local rpgplus = require 'rpgplus'
local storage = require 'rpgplus.storage'

local command = "&cSyntax: /getkeyv ('player') 'key'"

rpgplus.playercommand("getkeyv", function(sender, cmd, player, key)
	if sender:hasPermission("rpgplus.command.getkeyv") then
		if player ~= nil and key ~= nil then
			rpgplus.sendMessage(sender, player .. ":  " .. key .. " = " .. storage.of(player).get(key))
		elseif player ~= nil and key == nil then
			rpgplus.sendMessage(sender, sender:getName() .. ":  " .. player .. " = " .. storage.of(sender).get(player))
		else
			rpgplus.sendMessage(sender, command)
		end
	end
end)