local rpgplus = require 'rpgplus'

local playerUtil = require 'util/player'

rpgplus.playercommand("resetstats", function(sender, cmd, args)
	if sender:hasPermission("rpgplus.command.resetstats") then
		playerUtil.setDefaultStorage(sender)
	end
end)