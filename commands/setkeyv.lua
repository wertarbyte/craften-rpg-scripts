local rpgplus = require 'rpgplus'
local storage = require 'rpgplus.storage'

local command = "&cSyntax: /setkeyv ('player') 'key' 'value'"

rpgplus.playercommand("setkeyv", function(sender, cmd, player, key, value)
	if sender:hasPermission("rpgplus.command.setkeyv") then
		if player ~= nil and key ~= nil and value ~= nil then
			storage.of(player).set(key, value)
			rpgplus.sendMessage(sender, player .. ":  " .. key .. " = " .. storage.of(player).get(key))
		elseif player ~= nil and key ~= nil and value == nil then
			storage.of(sender).set(player, key)
			rpgplus.sendMessage(sender, sender:getName() .. ":  " .. player .. " = " .. storage.of(sender).get(player))
		else
			rpgplus.sendMessage(sender, command)
		end
	end
end)