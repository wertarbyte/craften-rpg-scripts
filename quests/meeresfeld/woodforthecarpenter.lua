local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'
local storage = require 'rpgplus.storage'
local scheduler = require 'rpgplus.scheduler'
local trading = require 'rpgplus.trading'

local playerUtil = require 'util/player'
local inventory = require 'rpgplus.inventory'
local bronzecoin = require 'items/money/bronzecoin'
local oakwood = require 'items/misc/oakwood'
local oakplanks = require 'items/misc/oakplanks'

local carpenter = entities.spawn("villager", {
	world="Meeresfeld", x=215.5, y=87, z=-100.5,
	invulnerable=true,
	name="&aKirk",
	secondName="&bQuest NPC",
	movementType="local",
	profession="farmer"})
	
carpenter:on("player.interact.entity", function(event)
	event:setCancelled(true)
	
	local playerstorage = storage.of(event:getPlayer())
	
	if playerstorage.get("quests.woodforthecarpenter.completed", false) == false then
		if playerstorage.get("quests.woodforthecarpenter.stage") == nil or playerstorage.get("quests.woodforthecarpenter.stage") == 0 then
			playerstorage.set("quests.woodforthecarpenter.stage", 1)
			playerstorage.set("quests.woodforthecarpenter.completed", false)
			
			carpenter:tell(event:getPlayer(), "&a[1/6] &fWas willst du hier Bursche? Steh nicht im Weg rum!")
			
			scheduler.delay(30, function()
				carpenter:tell(event:getPlayer(), "&a[2/6] &fTut mir leid, wenn ich etwas schroff bin, aber ich stehe gerade unter einem ziemlichen Zeitdruck.")
				scheduler.delay(30, function()
					carpenter:tell(event:getPlayer(), "&a[3/6] &fIch habe einen wichtigen Auftrag bekommen, aber ich fürchte ich werde nicht mehr fertig, wenn ich keine Hilfe bekomme.")
					scheduler.delay(30, function()
						carpenter:tell(event:getPlayer(), "&a[4/6] &fKönntest du mir villeicht helfen?")
						scheduler.delay(30, function()
							carpenter:tell(event:getPlayer(), "&a[5/6] &fIch habe gehört, dass vor der Stadt mometan viele Bäume gefällt werden.")
							scheduler.delay(30, function()
								carpenter:tell(event:getPlayer(), "&a[6/6] &fKönntest du dort hingehen und fragen, ob du den Holzfällern etwas Eichenholz abkaufen kannst?")
								rpgplus.sendMessage(event:getPlayer(), "&7Quest 'Holz für den Tischler' gestartet. [Bringe dem Tischler &b4 Eichenholz&7]")
							end)
						end)
					end)
				end)
			end)
			
		else
			if playerstorage.get("quests.woodforthecarpenter.stage") == 1 then
				carpenter:tell(event:getPlayer(), " &fHast du das Holz?")
				if inventory.hasItems(event:getPlayer(), oakwood(4)) then
					carpenter:tell(event:getPlayer(), " &fAhh gut du hast es! Kannst du es auch noch zum Sägewerk bringen? Ich kann nur Bretter gebrauchen und habe jetzt keine Zeit mehr diese selber zu machen! Der Besitzter vom Sägewerk heißt Herbert er sollte dir das Holz zu Bretter machen, wenn du ihn bittest.")
					carpenter:tell(event:getPlayer(), " &fDas Sägewerk befindet sich auf der anderen Seite der Insel, folge einfach dem Weg und überquere beide Flüsse. Das Sägewerk gehört zu einem Bauernhof. Nach der zweiten Brücke solltest du diesen schon sehen können.")
					carpenter:tell(event:getPlayer(), " &fDas Geld für das Holz und das Sägewerk gebe ich dir wieder nachdem du mir die Bretta gebracht hast.")
					rpgplus.sendMessage(event:getPlayer(), "&7[Bringe das Holz zum Sägewerk]")
					playerstorage.set("quests.woodforthecarpenter.stage", 2)
				else 
					carpenter:tell(event:getPlayer(), " &fBeeile dich bitte und bringe mir das Holz, es ist wirklich wichtig!")
				end
			elseif playerstorage.get("quests.woodforthecarpenter.stage") == 2 then
				if inventory.hasItems(event:getPlayer(), oakplanks(4)) then
					playerstorage.set("quests.woodforthecarpenter.stage", 3)
				
					carpenter:tell(event:getPlayer(), "&a[1/2] &fOhh du hast die Bretter! Endlich! Vielen vielen Dank! Nun werde ich es vermutlich schaffen den Auftrag rechtzeitig abzuschließen!")
					inventory.takeItems(event:getPlayer(), oakplanks(4))
					scheduler.delay(30, function()
						carpenter:tell(event:getPlayer(), "&a[2/2] &fHier ist das versprochene Geld! Und noch eine kleine Belohung oben drauf!")
						rpgplus.sendMessage(event:getPlayer(), "&7Quest 'Holz für den Tischler' abgeschlossen. (+33 XP +35 Bronzemünzen)")
						playerstorage.set("quests.woodforthecarpenter.completed", true)
						inventory.giveItems(event:getPlayer(), bronzecoin(35))
						playerUtil.givePlayerXp(event:getPlayer(), 33)
					end)
				else	
					carpenter:tell(event:getPlayer(), "Warst du schon bei dem Sägewerk? Nein?! Dann beeile dich bitte!")
				end
			end
		end
	else
		carpenter:tell(event:getPlayer(), {"Vielen Dank nochmal für deine Hilfe!", "Mit deiner Hilfe konnte ich den Auftrag noch rechzeitig fertigstellen!"})
	end
	
end)

local sawmill = entities.spawn("villager", {
	world="Meeresfeld", x=-325.5, y=84, z=-218.5,
	invulnerable=true,
	name="&aHerbert",
	secondName="&bQuest NPC",
	movementType="local",
	profession="farmer"})

sawmill:on("player.interact.entity", function(event)
	event:setCancelled(true)
	
	local playerstorage = storage.of(event:getPlayer())
	if playerstorage.get("quests.woodforthecarpenter.completed", false) == false then
		if playerstorage.get("quests.woodforthecarpenter.stage", 0) == 2 then
			sawmill:tell(event:getPlayer(), "Du willst, dass ich dir das Eichenholz zu Bretter zersäge? Das kostet aber!")
			rpgplus.sendMessage(event:getPlayer(), "&7[Lasse das Eichenholz zu Bretter zersägen und bringe diese zu Kirk zurück.]")
			scheduler.delay(40, function()
				trading.open(event:getPlayer(), {
					{oakwood(1), bronzecoin(2), oakplanks(1)}
				})
			end)
			return
		end
	end
	trading.open(event:getPlayer(), {
		{oakwood(1), bronzecoin(2), oakplanks(1)}
	})
end)