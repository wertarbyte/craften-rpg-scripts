local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'
local storage = require 'rpgplus.storage'
local scheduler = require 'rpgplus.scheduler'
local playerUtil = require 'util/player'
local inventory = require 'rpgplus.inventory'
local bronzecoin = require 'items/money/bronzecoin'

local villager = entities.spawn("villager", {
	world="tutorial", x=240, y=67, z=203,
	invulnerable=true,
	name="&aFranz",
	secondName="&bQuest NPC",
	movementType="local",
	profession="farmer"})
		
villager:on("player.interact.entity", function(event)
	event:setCancelled(true)
	
	local playerstorage = storage.of(event:getPlayer())
	
	if playerstorage.get("quests.franzscat.completed", false) == false then
		if playerstorage.get("quests.franzscat.stage") == nil then
			playerstorage.set("quests.franzscat.stage", 1)
			playerstorage.set("quests.franzscat.completed", false)
			
			villager:tell(event:getPlayer(), "&a[1/3] &fHallo Fremder! Meine Katze ist gerade in diese Höhle gelaufen und kommt nicht mehr heraus.")
			
			scheduler.delay(30, function()
				villager:tell(event:getPlayer(), "&a[2/3] &fIch traue mich nicht herein, da gefährliche Geräusche zu hören waren.")
				scheduler.delay(30, function()
					villager:tell(event:getPlayer(), "&a[3/3] &fDu siehst mutig aus und kannst sie bestimmt retten.")
					rpgplus.sendMessage(event:getPlayer(), "&7Quest 'Franz's Katze' gestartet.")
				end)
			end)
		else
			if playerstorage.get("quests.franzscat.stage") == 1 then
				villager:tell(event:getPlayer(), {"Konntest du meine Katze finden?", "Na los, beeeeil dich! Die arme Katze."})
			end
			if playerstorage.get("quests.franzscat.stage") == 2 then
				
				playerstorage.set("quests.franzscat.stage", 3)
				
				villager:tell(event:getPlayer(), "&a[1/3] &fAh, da ist sie ja.")
			
				scheduler.delay(30, function()
					villager:tell(event:getPlayer(), "&a[2/3] &fVielen Dank, ich kann dir das nie wieder gut machen.")
					scheduler.delay(30, function()
						villager:tell(event:getPlayer(), "&a[3/3] &fHier ist noch etwas Geld. Ich habe leider nicht mehr was ich dir geben kann...")
						rpgplus.sendMessage(event:getPlayer(), "&7Quest 'Franz's Katze' abgeschlossen. (+21 XP +10 Bronzemünzen)")
						playerstorage.set("quests.franzscat.completed", true)
						inventory.giveItems(event:getPlayer(), bronzecoin(10))
						playerUtil.givePlayerXp(event:getPlayer(), 21)
					end)
				end)
			end
		end
	else 
		villager:tell(event:getPlayer(), {"Hallo! Wie geht's dir so?", "Meine Katze hat sich wieder erhohlt.", "Nochmals vielen Dank, dass du mir geholfen hast."})
	end
	
end)

local cat = entities.spawn("OCELOT", {
	world="tutorial", x=250.5, y=68, z=203.5,
	invulnerable=true,
	name="&aFranz's Katze",
	secondName="&bQuest NPC",
	movementType="local"})

cat.type = "SIAMESE_CAT"
	
cat:on("player.interact.entity", function(event)
	event:setCancelled(true)
	
	cat:tell(event:getPlayer(), "Miauuuu!")
	local playerstorage = storage.of(event:getPlayer())
	if playerstorage.get("quests.franzscat.completed", false) == false then
		if playerstorage.get("quests.franzscat.stage", 0) == 1 then
			playerstorage.set("quests.franzscat.stage", 2)
		end
	end
end)