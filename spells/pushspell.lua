local rpgplus = require 'rpgplus'
local scheduler = require 'rpgplus.scheduler'
local particle = require 'rpgplus.particle'
local sound = require 'rpgplus.sound'
local entities = require 'rpgplus.entities'
local array = require 'util/array'

local itemstack = require 'items/itemstack'

local exports = {}

local name = "Abstoßungszauber"

local item = {type="skull_item", data=3, name="&r" .. name,
 texture="eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDAyYTg0OTFjYjZkZGViN2I5MjU5MjNlNDRkYzYyNGJhN2JmNzVlYmUzODI4YzI0Njk4YTVhZGU2NTlhOTQ2In19fQ==",
 lore={"&7Der Abstoßungszauber stößt alle Mobs,", "&7welche sich nah an dir befinden,", "&7weg und lähmt diese auch für kurze Zeit."}}

exports.name = name
 
exports.item = item

exports.storageName = "push"

local function push(event)
					local nearbyEntities = entities.getNearby(event:getPlayer():getLocation(), 5)
					local player = event:getPlayer()
					
					local playerLoc = player:getLocation()
					
					sound.playSound(player, "ENDERDRAGON_WINGS")
					
					local i = 0
					while i < array.size(nearbyEntities) do
						i = i + 1
						
						local bukkitEntity = nearbyEntities[i].bukkitEntity
						local loc = bukkitEntity:getLocation()
						
						local x1 = loc:getX()
						local y1 = loc:getY()
						local z1 = loc:getZ()
						
						local x2 = playerLoc:getX()
						local y2 = playerLoc:getY()
						local z2 = playerLoc:getZ()
						
						local xd = x1 - x2
						local yd = y1 - y2
						local zd = z1 - z2
						
						local factor = (3*2)/loc:distanceSquared(playerLoc)
						
						bukkitEntity:setVelocity(luajava.newInstance("org.bukkit.util.Vector", xd*factor, yd*factor/1.5 + 0.3, zd*factor))
						
					end
					
					local t = math.pi/2
					
					local location = {world=player:getLocation():getWorld():getName(), x=player:getLocation():getX(), y=player:getLocation():getY(), z=player:getLocation():getZ()}
					
					local task = 0
					
					local taskID = scheduler.repeating(0, 1, function()
					
						t = t + math.pi/5
						
						local theta = 0
						while theta <= 2*math.pi do
							
							local x = t*math.cos(theta)/2
							local y = 1.5*math.exp(-0.1*t) * math.sin(t) + 1.5
							local z = t*math.sin(theta)/2
							
							location.x = location.x + x
							location.y = location.y + y
							location.z = location.z + z
							
							particle.spawn(location, "EXPLOSION", {speed=0, count=1, id=0, data=0, radius=100, offset={x=0, y=0, z=0}})
							
							location.x = location.x - x
							location.y = location.y - y
							location.z = location.z - z
							
							theta = theta + math.pi/16
						end
						
						if t > 10 then
							scheduler.cancel(task)
						end
						
					end)
					task = taskID
		
end

exports.init = function()
	rpgplus.on("player.interact", function(event)
		if itemstack.getPlainName(event:getItem()) == name then
			if event:getPlayer():isSneaking() == true then
				if event:getAction():toString() == "RIGHT_CLICK_AIR" or event:getAction():toString() == "RIGHT_CLICK_BLOCK" then
					event:setCancelled(true)
					push(event)
				end
			end
		end
	end)
	
	rpgplus.on("player.interact.entity", function(event)
		if itemstack.getPlainName(event:getPlayer():getItemInHand()) == name then
			if event:getPlayer():isSneaking() == true then
				event:setCancelled(true)
				push(event)
			end
		end
	end)
end

return exports;