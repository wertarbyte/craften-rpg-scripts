local rpgplus = require 'rpgplus'
local scheduler = require 'rpgplus.scheduler'
local particle = require 'rpgplus.particle'
local sound = require 'rpgplus.sound'
local entities = require 'rpgplus.entities'
local storage = require 'rpgplus.storage'

local array = require 'util/array'
local itemstack = require 'items/itemstack'
local chatcolor = require 'util/chatcolor'

local mathUtil = require 'util/math'

local exports = {}

local name = "Feuerzauber"

local item = {type="skull_item", data=3, name="&r" .. name,
  texture="eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWMyZTlkODM5NWNhY2Q5OTIyODY5YzE1MzczY2Y3Y2IxNmRhMGE1Y2U1ZjNjNjMyYjE5Y2ViMzkyOWM5YTExIn19fQ=="}

exports.name = name
 
exports.item = item

exports.storageName = "fire"

local function endSpell(location, player)
	particle.spawn(location, "EXPLOSION_LARGE", {speed=0, count=1, id=0, data=0, radius=100, offset={x=0, y=0, z=0}})
	
	local nearby = entities.getNearby(location, 2.5)
	
	local damage = storage.get("spells.fire.level." .. storage.of(player).get("spells.fire.level", 1) .. ".damage", 9)
	
	for i=1,array.size(nearby) do
		local bukkitEntity = nearby[i].bukkitEntity
		bukkitEntity:setFireTicks(0)
		bukkitEntity:damage(0)
		
		local secondName = chatcolor.removeColorCodes(chatcolor.removeColorCodes(string.gsub(nearby[i].secondName, "❤", ""), "§"), "&")
		
		local health = tonumber(secondName)

		if health == nil then
		
		else
			health = health - damage
			nearby[i].bukkitEntity:damage(0, player)
			if health <= 0 then
				nearby[i]:kill()
			
				-- TODO give xp to player
				--save xp in storage
		
			end
			nearby[i].secondName = "&c" .. health .. "❤"
		end
			
	end
	
end

local function fire(event)
					
					local player = event:getPlayer()
					
					sound.playSound(player, "FIREWORK_LAUNCH")
					
					local level = storage.of(player).get("spells.fire.level", 1)
					
					local t = 0
					
					local location = {world=player:getLocation():getWorld():getName(), x=player:getLocation():getX(), y=player:getEyeLocation():getY(), z=player:getLocation():getZ()}
					
					local pitch = player:getLocation():getPitch()+90
					
					local yaw = player:getLocation():getYaw()
					
					local task = 0
					
					local taskID = scheduler.repeating(0, 1, function()
					
						for i=1,15,1 do
							t = t + 0.1
							
							local x = 0
							local y = t
							local z = 0
							
							local rotatedVector = mathUtil.rotate({x=x, y=y, z=z}, pitch, yaw)
							
							location.x = location.x + rotatedVector.x
							location.y = location.y + rotatedVector.y
							location.z = location.z + rotatedVector.z
							
							if player:getWorld():getBlockAt(math.floor(location.x), math.floor(location.y), math.floor(location.z)):getType():toString() == "AIR" then
								if array.size(entities.getNearby(location, 1.5)) > 0 then
									scheduler.cancel(task)
									endSpell(location, player)
									break
								end
							else
								scheduler.cancel(task)
								endSpell(location, player)
								break
							end
							
							
							location.x = location.x - rotatedVector.x
							location.y = location.y - rotatedVector.y
							location.z = location.z - rotatedVector.z
							
						end
						
						t = t - 1.5
						
						t = t + 1.0
						
						local x = 0
						local y = t
						local z = 0
						
						local rotatedVector = mathUtil.rotate({x=x, y=y, z=z}, pitch, yaw)
						
						location.x = location.x + rotatedVector.x
						location.y = location.y + rotatedVector.y
						location.z = location.z + rotatedVector.z
						
						particle.spawn(location, "FIREWORKS_SPARK", {speed=0, count=1, id=0, data=0, radius=100, offset={x=0, y=0, z=0}})
						
						location.x = location.x - rotatedVector.x
						location.y = location.y - rotatedVector.y
						location.z = location.z - rotatedVector.z
						
						if level > 1 then
							for i=0,2,1 do
								local x = math.cos(t/4)/2
								local y = t
								local z = math.sin(t/4)/2
						
								local rotatedVector = mathUtil.rotate({x=x, y=y, z=z}, pitch, yaw)
						
								location.x = location.x + rotatedVector.x
								location.y = location.y + rotatedVector.y
								location.z = location.z + rotatedVector.z
						
								particle.spawn(location, "FLAME", {speed=0, count=1, id=0, data=0, radius=100, offset={x=0, y=0, z=0}})
						
								location.x = location.x - rotatedVector.x
								location.y = location.y - rotatedVector.y
								location.z = location.z - rotatedVector.z
								
								t = t + 0.5
							end
							t = t - 1
						end
						if level > 2 then
							for i=0,2,1 do
								local x = math.cos(t/2)/2
								local y = t
								local z = math.sin(t/2)/2
						
								local rotatedVector = mathUtil.rotate({x=x, y=y, z=z}, pitch, yaw)
						
								location.x = location.x + rotatedVector.x
								location.y = location.y + rotatedVector.y
								location.z = location.z + rotatedVector.z
						
								particle.spawn(location, "FLAME", {speed=0, count=1, id=0, data=0, radius=100, offset={x=0, y=0, z=0}})
						
								location.x = location.x - rotatedVector.x
								location.y = location.y - rotatedVector.y
								location.z = location.z - rotatedVector.z
								
								t = t + 0.5
							end
							t = t - 1
						end
						
						t = t + 0.5
						
						local x = 0
						local y = t
						local z = 0
						
						local rotatedVector = mathUtil.rotate({x=x, y=y, z=z}, pitch, yaw)
						
						location.x = location.x + rotatedVector.x
						location.y = location.y + rotatedVector.y
						location.z = location.z + rotatedVector.z
						
						particle.spawn(location, "LAVA_POP", {speed=0, count=1, id=0, data=0, radius=100, offset={x=0, y=0, z=0}})
						
						location.x = location.x - rotatedVector.x
						location.y = location.y - rotatedVector.y
						location.z = location.z - rotatedVector.z
						
						
						if t > 80 then
							scheduler.cancel(task)
							
							location.x = location.x + rotatedVector.x
							location.y = location.y + rotatedVector.y
							location.z = location.z + rotatedVector.z
							endSpell(location, player)
						end
						
					end)
					task = taskID
		
end

exports.init = function()
	rpgplus.on("player.interact", function(event)
		if itemstack.getPlainName(event:getItem()) == name then
			if event:getPlayer():isSneaking() == true then
				if event:getAction():toString() == "RIGHT_CLICK_AIR" or event:getAction():toString() == "RIGHT_CLICK_BLOCK" then
					event:setCancelled(true)
					fire(event)
				end
			end
		end
	end)
	
	rpgplus.on("player.interact.entity", function(event)
		if itemstack.getPlainName(event:getPlayer():getItemInHand()) == name then
			if event:getPlayer():isSneaking() == true then
				event:setCancelled(true)
				fire(event)
			end
		end
	end)
	
end

return exports