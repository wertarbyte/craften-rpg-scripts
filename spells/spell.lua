local rpgplus = require 'rpgplus'
local inventory = require 'rpgplus.inventory'
local storage = require 'rpgplus.storage'

local array = require 'util/array'
local itemstack = require 'items/itemstack'
local chatcolor = require 'util/chatcolor'

local pushspell = require 'spells/pushspell'
local firespell = require 'spells/firespell'

local spellList = {}

local exports = {}

exports.getSpell = function(spellName)
	for i=1,array.size(spellList),1 do
		if chatcolor.removeColorCodes(spellList[i].name, "&") == spellName then
			return spellList[i]
		end
	end
	
	return nil
end

local function openSpellInv(player)
	
	local playerstorage = storage.of(player)
	
	local items = {}
	
	local rootStorage = "spells."
	
	local enabledSpells = playerstorage.get(rootStorage .. "countOfEnabledSpells", 0)
	
	local maxSpells = storage.get("spells.maxEnabledSpells", 3)
	
	local lockedSpell = {type="barrier", name = "&4&lZauber noch nicht freigeschaltet!"}
	local enabledSpell = {type="ink_sack", data=10, name="&a&lAktiviert"}
	local disabledSpell = {type="ink_sack", data=8, name="&8&lDeaktiviert"}
	
	items[45] = {type="barrier", name="&rSchließen"}
	
	if playerstorage.get(rootStorage .. "push.unlock", false) then
		items[0] = pushspell.item
		if playerstorage.get(rootStorage .. "push.enabled", false) then
			items[9] = enabledSpell
		else
			items[9] = disabledSpell
		end
	else
		items[0] = lockedSpell
	end
	
	if playerstorage.get(rootStorage .. "fire.unlock", false) then
		items[1] = firespell.item
		if playerstorage.get(rootStorage .. "fire.enabled", false) then
			items[10] = enabledSpell
		else
			items[10] = disabledSpell
		end
	else
		items[1] = lockedSpell
	end
	
	local inv = inventory.openChest(player, 6, "&4&lZauber &2&lAuswahl", items)
	
	inv:on("click", function(event)
		event:setCancelled(true)
		local item = event:getCurrentItem()
			
		if event:getClickedInventory():getType():toString() == "CHEST" then
			if item:getType():toString() == "INK_SACK" then
				
				if item:getDurability() == enabledSpell.data then
					inv:setItem(event:getSlot(), disabledSpell)
					enabledSpells = enabledSpells - 1
					
					local spellItem = event:getClickedInventory():getItem(event:getSlot() - 9)
					local spell = exports.getSpell(itemstack.getPlainName(spellItem))
					
					inventory.takeItems(player, spell.item)
					
					playerstorage.set(rootStorage .. spell.storageName .. ".enabled", false)
						
				elseif item:getDurability() == disabledSpell.data then
					if enabledSpells < maxSpells then
						inv:setItem(event:getSlot(), enabledSpell)
						enabledSpells = enabledSpells + 1
						
						
						local spellItem = event:getClickedInventory():getItem(event:getSlot() - 9)
						local spell = exports.getSpell(itemstack.getPlainName(spellItem))
						
						inventory.giveItems(player, spell.item)
						
						playerstorage.set(rootStorage .. spell.storageName .. ".enabled", true)
						
					end
				end
				
				playerstorage.set(rootStorage .. "countOfEnabledSpells" , enabledSpells)
				
			end
		end
		
	end)
	
end

exports.init = function() 
	pushspell.init()
	firespell.init()
	
	spellList = {pushspell, firespell}
	
	rpgplus.playercommand("spells", function(sender, cmd, args)
		openSpellInv(sender)
	end)
	
	rpgplus.playercommand("zauber", function(sender, cmd, args)
		openSpellInv(sender)
	end)
	
	rpgplus.on("player.item.drop", function(event)
		if exports.getSpell(itemstack.getPlainName(event:getItemDrop():getItemStack())) == nil then
		
		else
			event:setCancelled(true)
		end
	end)
	
end

return exports