local rpgplus = require 'rpgplus'
local playerUtil = require 'util/player'
local locationUtil = require 'util/location'
local mathUtil = require 'util/math'
local chatcolor = require 'util/chatcolor'
local weapons = require 'items/weapons/weapons'
local itemstack = require 'items/itemstack'

local function attackOnlyPlayers(entity)
	entity:on("entity.target", function(event)
		if not playerUtil.isPlayer(event:getTarget()) then
			event:setCancelled(true)
		end
	end)
end

local function attackNearbyPlayers(entity, range)
	rpgplus.on("player.move", function(event)
		if mathUtil.distanceSquared(locationUtil.convertJavaToLua(event:getPlayer():getLocation()), entity.location) <= range*range then
			if event:getPlayer():getGameMode():toString() == "SURVIVAL" then
				entity.target = event:getPlayer()	
			end 
			if event:getPlayer():getGameMode():toString() == "ADVENTURE" then
				entity.target = event:getPlayer()
			end
		end
	end)
end

local function registerAtackHandler(entity, damage)
	entity:on("entity.attack.entity", function(event)
		if playerUtil.isPlayer(event:getEntity()) then
			event:setDamage(0)
			event:getEntity():damage(0)
			local damageModifier = luajava.bindClass("org.bukkit.event.entity.EntityDamageEvent$DamageModifier")
			event:setDamage(damageModifier.ARMOR, 0)
			event:setDamage(damageModifier.MAGIC, 0)
			
			local oldPlayerHealth = playerUtil.getHealth(event:getEntity())
			local playerArmorDefense = playerUtil.getTotalArmorDefensePoints(event:getEntity())
			
			local newDamage = damage - playerArmorDefense - math.floor(playerUtil.skill(event:getEntity(), "defense")/2)
			
			if newDamage < 1 then
				newDamage = 1
			end
			
			local newPlayerhealth = oldPlayerHealth - newDamage
			
			playerUtil.setHealth(event:getEntity(), newPlayerhealth)
			
		end
	end)
end

local function registerDamageHandler(entity, luaEntity)
	entity:on("entity.damage.byEntity", function(event)
		if playerUtil.isPlayer(event:getDamager()) then
			event:setDamage(0)
			local damageModifier = luajava.bindClass("org.bukkit.event.entity.EntityDamageEvent$DamageModifier")
			event:setDamage(damageModifier.ARMOR, 0)
			event:setDamage(damageModifier.MAGIC, 0)
			local secondName = chatcolor.removeColorCodes(chatcolor.removeColorCodes(string.gsub(entity.secondName, "❤", ""), "§"), "&")
			local health = tonumber(secondName)
			if weapons.isWeapon(itemstack.getDisplayName(event:getDamager():getItemInHand())) then
				local weapon = weapons.getWeapon(itemstack.getDisplayName(event:getDamager():getItemInHand()))
				local damage = weapon.getDamage() + playerUtil.skill(event:getDamager(), "sword")
				health = health - damage
			else 
				health = health - 1
			end
			
			if health <= 0 then
				entity:kill()
				local xp = luaEntity.getXp()
				-- TODO calculate xp
				--save xp in storage
				playerUtil.givePlayerXp(event:getDamager(), xp)
			end
			
			entity.secondName = "&c" .. health .. "❤"
			
		end
	end)
end

return {attackOnlyPlayers = attackOnlyPlayers, attackNearbyPlayers = attackNearbyPlayers, registerAtackHandler = registerAtackHandler, registerDamageHandler = registerDamageHandler}