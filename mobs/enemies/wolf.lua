local rpgplus = require 'rpgplus'
local scheduler = require 'rpgplus.scheduler'
local storage = require 'rpgplus.storage'
local playerUtil = require 'util/player'
local itemstack = require 'items/itemstack'
local weapons = require 'items/weapons/weapons'
local chatcolor = require 'util/chatcolor'
local locationUtil = require 'util/location'
local bronzecoin = require 'items/money/bronzecoin'
local mathUtil = require 'util/math'
local rottenflesh = require 'items/junk/rottenflesh'
local enemies = require 'mobs/enemies/enemies'
local arrayUtil = require 'util/array'
local applyDefaults = require 'util/applyDefaults'
local entities = require 'rpgplus.entities'

local applyDefaultWolfOptions = applyDefaults({
	maxhealth = 20,
	level = 3,
	name = "Wilder Wolf",
	damage = 25,
	xpDrops = 6
})

local function spawn(options, locations)
	applyDefaultWolfOptions(options)
	local displayName = "&6[Lvl. &c" .. (options.level) .. "&6] &4" .. (options.name)

	local location = arrayUtil.getRandomIndex(locations)
	local wolf = entities.spawn("wolf", {
		world = location.world, x = location.x, y = location.y, z = location.z,
		invulnerable = false,
		name = displayName,
		movementType = "normal"
	})
	
	wolf.secondName = "&c" .. (options.maxhealth) .. "❤"
	wolf:on("entity.changeblock", function(event)
		event:setCancelled(true)
	end)
	
	enemies.attackOnlyPlayers(wolf)
	
	wolf:on("entity.death", function(event)
		local percent = math.random(0, 1000)
		if percent < 650 then
			rpgplus.dropItems(event:getEntity():getLocation(), bronzecoin(1))
		end
		if percent > 600 then
			rpgplus.dropItems(event:getEntity():getLocation(), rottenflesh(1))
		end
		
		scheduler.delay(40 + math.random(0, 60), function()
			spawn(options, locations)
		end)
	end)
	
	wolf:on("entity.tame", function(event)
		event:setCancelled(true)
	end)
	
	enemies.registerAtackHandler(wolf, damage)
	
	enemies.attackNearbyPlayers(wolf, 25)
	
	wolf.rpg = {
		level = options.level,
		xp = options.xpDrops,
		damage = options.damage,

		-- for compatibility with existing code, but this should be removed later
		getLevel = function () return level end,
		getName = function () return name end,
		getDisplayName = function () return displayName end,
		getMaxHealth = function () return options.maxhealth end,
		getDamage = function () return options.damage end,
		getXp = function () return xpDrops end
	}
	
	-- wolf.rpg has exactly the functions that registerDamageHandler expects (e.g. getXp)
	-- coincidence? i don't think so. :D
	enemies.registerDamageHandler(wolf, wolf.rpg)

	return wolf
end

return {spawn = spawn}
