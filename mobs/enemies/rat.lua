local rpgplus = require 'rpgplus'
local scheduler = require 'rpgplus.scheduler'
local storage = require 'rpgplus.storage'
local playerUtil = require 'util/player'
local itemstack = require 'items/itemstack'
local weapons = require 'items/weapons/weapons'
local chatcolor = require 'util/chatcolor'
local locationUtil = require 'util/location'
local bronzecoin = require 'items/money/bronzecoin'
local enemies = require 'mobs/enemies/enemies'
local arrayUtil = require 'util/array'
local entities = require 'rpgplus.entities'

local maxhealth = 13
local level = 1

local name = "Ratte"
local displayname = "&6[Lvl. &c" .. level .. "&6] &4" .. name

local damage = 12

local xpDrops = 2

local function spawn(world, positions)
	
	local position = arrayUtil.getRandomIndex(positions)
	local x = position.x
	local y = position.y
	local z = position.z
	
	local rat = entities.spawn("silverfish", {
		world=world, x=x, y=y, z=z,
		invulnerable=false,
		name=displayname,
		movementType="normal"})
	
	rat.secondName = "&c" .. maxhealth .. "❤"
	rat:on("entity.changeblock", function(event)
		event:setCancelled(true)
	end)
	
	enemies.attackOnlyPlayers(rat)
	
	enemies.registerDamageHandler(rat, require 'mobs/enemies/rat')
	
	rat:on("entity.death", function(event)
		local percent = math.random(0, 1000)
		if percent < 650 then
			rpgplus.dropItems(locationUtil.convertJavaToLua(event:getEntity():getLocation()), bronzecoin(1))
		end
		
		scheduler.delay(35 + math.random(0, 50), function()
			spawn(world, positions)
		end)
		
	end)
	
	enemies.registerAtackHandler(rat, damage)
	
	return rat
	
end

local function getLevel()
	return level
end

local function getName()
	return name
end

local function getDisplayName()
	return displayname
end

local function getMaxHealth()
	return maxhealth
end

local function getDamage()
	return damage
end	

local function getXp()
	return xpDrops
end

return {spawn = spawn, getLevel = getLevel, getName = getName, getDamage = getDamage, getDisplayName = getDisplayName, getMaxHealth = getMaxHealth, getXp = getXp}