local rpgplus = require 'rpgplus'
local trading = require 'rpgplus.trading'
local rottenflesh = require '../items/junk/rottenflesh'
local string = require '../items/junk/string'
local entities = require 'rpgplus.entities'

local bronzecoin = require '../items/money/bronzecoin'

local function spawnJunkTrader(world, x, y, z)
	local junkTrader = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&dMüllhändler",
		profession="farmer"
	})

	junkTrader:on("player.interact.entity", function(event)
		trading.open(event:getPlayer(), {
			{rottenflesh(7), bronzecoin(1)},
			{string(6), bronzecoin(1)}
		})
		event:setCancelled(true)
	end)

	return junkTrader
end

return spawnJunkTrader
