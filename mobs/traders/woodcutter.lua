local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'
local trading = require 'rpgplus.trading'
local storage = require 'rpgplus.storage'
local scheduler = require 'rpgplus.scheduler'

local bronzecoin = require 'items/money/bronzecoin'
local oakwood = require 'items/misc/oakwood'
local birchwood = require 'items/misc/birchwood'
local sprucewood = require 'items/misc/sprucewood'
local junglewood = require 'items/misc/junglewood'

local function spawn(world, x, y, z)
	
	local trader = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&dHolzfäller",
		profession="farmer"
	})
	
	trader:on("player.interact.entity", function(event)
		event:setCancelled(true)
		
		local playerstorage = storage.of(event:getPlayer())

		if playerstorage.get("quests.woodforthecarpenter.completed", false) == false then
			if playerstorage.get("quests.woodforthecarpenter.stage") == 1 then
				trader:tell(event:getPlayer(), "Du brauchst Eichenholz? Kein Problem ich kann dir gerne welches verkaufen.")
				
				scheduler.delay(40, function()
					trading.open(event:getPlayer(), {
						{bronzecoin(5), oakwood(1)},
						{bronzecoin(2), birchwood(1)},
						{bronzecoin(3), sprucewood(1)},
						{bronzecoin(4), junglewood(1)}
					})
					rpgplus.sendMessage(event:getPlayer(), "&7Kaufe &b4 Eichenholz&7 und bringe sie zum Tischler.")
				end)
				
				return
			end
		end
		trading.open(event:getPlayer(), {
				{bronzecoin(2), birchwood(1)},
				{bronzecoin(3), sprucewood(1)},
				{bronzecoin(4), junglewood(1)},
				{bronzecoin(5), oakwood(1)}
		})
		
	end)
	
end

return spawn
