local rpgplus = require 'rpgplus'
local trading = require 'rpgplus.trading'
local entities = require 'rpgplus.entities'

local bronzecoin = require 'items/money/bronzecoin'
local silvercoin = require 'items/money/silvercoin'

local function spawn(world, x, y, z)
	local merchant = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&dGeldhändler",
		profession="farmer"
	})

	merchant:on("player.interact.entity", function(event)
		trading.open(event:getPlayer(), {
			{bronzecoin(64), silvercoin(1)},
			{silvercoin(1), bronzecoin(64)}
		})
		event:setCancelled(true)
	end)

	return merchant
end
return spawn;