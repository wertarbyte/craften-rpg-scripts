local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'

local function spawn(world, x, y, z)
	local villager = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&aTarimsfort Wächter",
		movementType="local",
		profession="blacksmith"})
		
	villager:on("player.interact.entity", function(event)
		event:setCancelled(true)
		
		if math.random(0, 100) < 10 then
			villager:tell(event:getPlayer(), {"Hast du irgentwas in der Nähe gesehen? Ich habe das Gefühl hier treibt irgentetwas sein Unwesen.", "Psst, sei leise, hörst du das auch? Hier stimmt irgentetwas nicht!", "Ich hoffe meine Ablösung kommt bald."})
		end
		
	end)
	villager:on("block.form.entity", function(event)
		event:setCancelled(true)
	end)
	villager:on("entity.changeblock", function(event)
		event:setCancelled(true)
	end)
end

return spawn