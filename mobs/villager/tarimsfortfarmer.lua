local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'

local function spawn(world, x, y, z)
	local villager = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&aTarimsfort Feldarbeiter",
		movementType="normal",
		profession="farmer"})
		
	villager:on("player.interact.entity", function(event)
		event:setCancelled(true)
		
		if math.random(0, 100) < 20 then
			villager:tell(event:getPlayer(), {"Das Leben hier ist hart, aber man kommt durch.", "Das Wetter war dieses Jahr gut, hoffentlich gibt es eine gute Ernte."})
		end
		
	end)
	villager:on("block.form.entity", function(event)
		event:setCancelled(true)
	end)
	villager:on("entity.changeblock", function(event)
		event:setCancelled(true)
	end)
end

return spawn