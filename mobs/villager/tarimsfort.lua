local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'

local function spawn(world, x, y, z)
	local villager = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&aTarimsfort Bewohner",
		movementType="normal",
		profession="farmer"})
		
	villager:on("player.interact.entity", function(event)
		event:setCancelled(true)
		
		if math.random(0, 100) < 33 then
			villager:tell(event:getPlayer(), {"Hallo, wie geht es dir?", "Das Leben hier ist hart, aber man kommt durch."})
		end
		
	end)
	villager:on("block.form.entity", function(event)
		event:setCancelled(true)
	end)
	villager:on("entity.changeblock", function(event)
		event:setCancelled(true)
	end)
end

return spawn