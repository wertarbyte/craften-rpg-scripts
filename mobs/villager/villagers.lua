local meeresfeld = require 'mobs/villager/meeresfeld'
local tarimsfort = require 'mobs/villager/tarimsfort'
local tarimsfortfarmer = require 'mobs/villager/tarimsfortfarmer'
local tarimsfortguard = require 'mobs/villager/tarimsfortguard'
local TutorialStadtwache = require 'mobs/villager/TutorialStadtwache'

-- Meeresfeld
meeresfeld("rpg", 30, 32, 565)

-- Tarimsfort
--Feldarbeiter
tarimsfortfarmer("tarimsfort", -49, 72, 152)
tarimsfortfarmer("tarimsfort", -41, 71, 151)
tarimsfortfarmer("tarimsfort", -52, 71, 145)
tarimsfortfarmer("tarimsfort", -38, 69, 134)
tarimsfortfarmer("tarimsfort", -19, 66, 105)
tarimsfortfarmer("tarimsfort", -33, 65, 94)
tarimsfortfarmer("tarimsfort", -21, 65, 85)
tarimsfortfarmer("tarimsfort", -16, 66, 77)
tarimsfortfarmer("tarimsfort", -20, 66, 69)
tarimsfortfarmer("tarimsfort", -26, 66, 65)
tarimsfortfarmer("tarimsfort", -31, 65, 67)
tarimsfortfarmer("tarimsfort", -57, 69, 56)
tarimsfortfarmer("tarimsfort", -55, 68, 53)
tarimsfortfarmer("tarimsfort", 52, 68, 56)

--Rest
tarimsfort("tarimsfort", -50, 68, 128)
tarimsfort("tarimsfort", -62, 64, 126)
tarimsfort("tarimsfort", -64, 64, 119)
tarimsfort("tarimsfort", -64, 64, 111)
tarimsfort("tarimsfort", -75, 64, 108)
tarimsfort("tarimsfort", -82, 65, 104)
tarimsfort("tarimsfort", -74, 65, 101)
tarimsfort("tarimsfort", -72, 64, 115)
tarimsfort("tarimsfort", -74, 64, 124)
tarimsfort("tarimsfort", -32, 68, 122)
tarimsfort("tarimsfort", -38, 71, 172)
tarimsfort("tarimsfort", -50, 64, 100)
tarimsfort("tarimsfort", -53, 64, 96)
tarimsfort("tarimsfort", -48, 64, 91)
tarimsfort("tarimsfort", -43, 64, 92)
tarimsfort("tarimsfort", -44, 64, 86)
tarimsfort("tarimsfort", -39, 65, 84)
tarimsfort("tarimsfort", -42, 65, 78)
tarimsfort("tarimsfort", -39, 65, 72)
tarimsfort("tarimsfort", -42, 65, 67)
tarimsfort("tarimsfort", -38, 66, 62)
tarimsfort("tarimsfort", -38, 66, 59)
tarimsfort("tarimsfort", -78, 65, 98)
tarimsfort("tarimsfort", -89, 64, 99)

--Turm Wächter
tarimsfortguard("tarimsfort", -66.5, 78, 140.5)
tarimsfortguard("tarimsfort", 161.5, 80, 128.5)
tarimsfortguard("tarimsfort", 145.5, 91, -85.5)
tarimsfortguard("tarimsfort", 35.5, 83, 116.5)
tarimsfortguard("tarimsfort", 27.5, 71, 259.5)
tarimsfortguard("tarimsfort", 5.5, 75, 192.5)
tarimsfortguard("tarimsfort", -16.5, 75, 67.5)
tarimsfortguard("tarimsfort", -45.5, 84, 17.5)
tarimsfortguard("tarimsfort", -61.5, 103, -92.5)
tarimsfortguard("tarimsfort", -99.5, 89, 67.5)

-- Tutorial
--TutorialStadtwache
TutorialStadtwache("tutorial", 227, 80, 221)
TutorialStadtwache("tutorial", 224, 79, 221)
TutorialStadtwache("tutorial", 110, 85, 67)
TutorialStadtwache("tutorial", 99, 87, 67)
TutorialStadtwache("tutorial", 118, 87, 205)
TutorialStadtwache("tutorial", 98, 87, 205)
