local rpgplus = require 'rpgplus'
local entities = require 'rpgplus.entities'

local function spawn(world, x, y, z, x1, y1, z1)
	local villager = entities.spawn("villager", {
		world=world, x=x, y=y, z=z,
		invulnerable=true,
		name="&aStadtwache",
		movementType="normal",
		profession="blacksmith"})

	villager:on("player.interact.entity", function(event)
		event:setCancelled(true)
		
		if math.random(0, 100) < 20 then
			villager:tell(event:getPlayer(), {"Hallo... Fremder", "Kann ich dir helfen?", "Ich habe auch noch anderes zu tun.", "Du siehst verdächtig aus, was hast du verbrochen?"})
		end
		
	end)
	villager:on("block.form.entity", function(event)
		event:setCancelled(true)
	end)
	villager:on("entity.changeblock", function(event)
		event:setCancelled(true)
	end)
end

return spawn