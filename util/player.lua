local rpgplus = require 'rpgplus'
local storage = require 'rpgplus.storage'
local actionbar = require 'rpgplus.actionbar'
local sound = require 'rpgplus.sound'
local inventory = require 'rpgplus.inventory'

local itemstack = require 'items/itemstack'
local armor = require 'items/armor/armor'
local system = require 'util/system'
local chatcolor = require 'util/chatcolor'

local function isPlayer(javaEntity)
	if javaEntity == nil then
		return false
	end

	if javaEntity:getType():toString() == "PLAYER" then
		return true
	else
		return false
	end
end

local function getTotalArmorDefensePoints(javaPlayer)
	local playerInventory = javaPlayer:getInventory()
	local totalDefense = 0
	
	local helmetName = itemstack.getDisplayName(playerInventory:getHelmet())
	local bootsName = itemstack.getDisplayName(playerInventory:getBoots())
	local leggingsName = itemstack.getDisplayName(playerInventory:getLeggings())
	local chestplateName = itemstack.getDisplayName(playerInventory:getChestplate())
	
	if armor.isArmor(helmetName) then
		totalDefense = totalDefense + armor.getArmor(helmetName).getDefense()
	end
	if armor.isArmor(bootsName) then
		totalDefense = totalDefense + armor.getArmor(bootsName).getDefense()
	end
	if armor.isArmor(leggingsName) then
		totalDefense = totalDefense + armor.getArmor(leggingsName).getDefense()
	end
	if armor.isArmor(chestplateName) then
		totalDefense = totalDefense + armor.getArmor(chestplateName).getDefense()
	end
	
	return totalDefense
end

local function isNew(player)
	local playerStorage = storage.of(player)
	if playerStorage.get("firstJoin", 0) == 0 then
		return true
	else
		return false
	end
end

local function getHealth(player)
	return storage.of(player).get("health", 100)
end	

local function updateActionbar(player)
	local msg = "&c❤" .. getHealth(player) .. "/" .. storage.of(player).get("maxHealth", 100)
	actionbar.set(player, msg)
end
local function setHealth(player, health)
	
	local maxHealth = storage.of(player).get("maxHealth", 100)
	
	local hearts = math.floor(20*(health/maxHealth))
	
	if hearts > 0 then
		player:setHealth(hearts)
	end
	
	if health <= 0 then
		player:setHealth(0)
		storage.of(player).set("health", maxHealth)
	else
		storage.of(player).set("health", health)
	end
	
	updateActionbar(player)
	
end

local function setMaxHealth(player, maxHealth)
	storage.of(player).set("maxHealth", maxHealth)
end

local function getMaxHealth(player)
	return storage.of(player).get("maxHealth", 100)
end	

local function setDefaultStorage(player)
	local playerStorage = storage.of(player)
	
	playerStorage.set("firstJoin", system.getSystemTime())
	setMaxHealth(player, 100)
	setHealth(player, 100)
	playerStorage.set("level", 1)
	playerStorage.set("xp", 0)
	playerStorage.set("totalXp", 0)
	playerStorage.set("activeSong", false)
	playerStorage.set("skillPoints", 0)
	playerStorage.set("totalSkillPoints", 0)
end

local function remainingSkillPoints(player)
	return storage.of(player).get("skillPoints", 0)
end

local function totalSkillPoints(player)
	return storage.of(player).get("totalSkillPoints", 0)
end

local function giveSkillPoints(player, points)
	
	local oldSkillPoints = remainingSkillPoints(player)
	local oldTotalSkillPoints = totalSkillPoints(player)
	
	local newSkillPoints = oldSkillPoints + points
	local newTotalSkillPoints = oldTotalSkillPoints + points
		
	if newSkillPoints < 0 or newTotalSkillPoints + points < 0 then
		newSkillPoints = 0
		newTotalSkillPoints = 0
	end
	
	local playerStorage = storage.of(player)

	playerStorage.set("skillPoints", newSkillPoints)
	playerStorage.set("totalSkillPoints", newTotalSkillPoints)
	
end
local function getExpAmountForLvl(level)
	local xp = storage.get("level." .. level .. ".xp", 0)
	if xp == 0 then
		if level == 1 then
			xp = 20;
		else
			local xpOfPreviousLvl = getExpAmountForLvl(level - 1)
			if level < 20 then
				xp = math.ceil(xpOfPreviousLvl * 1.4)
			elseif level < 50 then
				xp = math.ceil(xpOfPreviousLvl * 1.2)
			else 
				xp = math.ceil(xpOfPreviousLvl * 1.1)
			end
		end
		storage.set("level." .. level .. ".xp", xp)
	end
	return xp
end

local function updateXpBar(player)
	local playerStorage = storage.of(player)
	local lvl = playerStorage.get("level", 1)
	local xp = playerStorage.get("xp", 0)
	local neededXp = getExpAmountForLvl(lvl)
	player:setLevel(lvl)
	player:setExp(xp/neededXp)
end

local function givePlayerXp(player, amount)
	local playerStorage = storage.of(player)
	local currentXp = playerStorage.get("xp", 0)
	local currentLvl = playerStorage.get("level", 1)
	local currentTotalXp = playerStorage.get("totalXp", 0)
	
	local newTotalXp = currentTotalXp + amount
	local newXp = currentXp + amount
	local newLvl = currentLvl
	
	if newXp >= getExpAmountForLvl(currentLvl) then
		newXp = newXp - getExpAmountForLvl(currentLvl)
		newLvl = currentLvl + 1
		sound.playSound(player, "LEVEL_UP")
		
		giveSkillPoints(player, storage.get("skillPointsPerLevel", 3))
		
		rpgplus.sendMessage(player, "&6Du hast " .. storage.get("skillPointsPerLevel", 3) .. " Skillpunkte bekommen.")
		
	end
	
	playerStorage.set("totalXp", newTotalXp)
	playerStorage.set("xp", newXp)
	playerStorage.set("level", newLvl)
	
	updateXpBar(player)
	
end

local function getLevel(player)
	return storage.of(player).get("level")
end

local function getXp(player)
	return storage.of(player).get("xp")
end

local function getTotalXp(player)
	return storage.of(player).get("totalXp")
end

local function skill(player, skill)
	return storage.of(player).get("skills." .. skill, 0)
end

local function updateSkill(player, skill, level)
	storage.of(player).set("skills." .. skill, level)
end

local function openSkillInv(player, parentInv)
	local skillPoints = remainingSkillPoints(player)
	
	local items = {}
	
	items[36] = {type="skull_item", data=3, name="&rZurück", texture="eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2ViZjkwNzQ5NGE5MzVlOTU1YmZjYWRhYjgxYmVhZmI5MGZiOWJlNDljNzAyNmJhOTdkNzk4ZDVmMWEyMyJ9fX0="}
	items[4] = {type="skull_item", data=3, amount=skillPoints, name="&r&k...&r &b&9Skill Punkte: &r&k...", lore={"&dDu hast &c" .. skillPoints .. "&d Skillpunkte,", "&dwelche du noch verteilen kannst"}, texture="eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTc2MmExNWIwNDY5MmEyZTRiM2ZiMzY2M2JkNGI3ODQzNGRjZTE3MzJiOGViMWM3YTlmN2MwZmJmNmYifX19"}
	
	local swordSkill = skill(player, "sword")
	items[10] = {type="iron_sword", amount=swordSkill, name="&b&4Nahkampf", lore={"&7Der Nahkampf-Skill erhöht den Schaden,", "&7den du deinen Gegner, mit einer Nahkampfwaffe, zufügst.", "&6Dein aktueller Nahkampf-Skill beträgt: &4" .. swordSkill,
		"&6Effekt: &4+" .. swordSkill .. " &6Schaden",
		"&7Linksklick um den Skill um &41 &7Level zuerhöhen", "&7Rechtsklick um den Skill um &43 &7Level zuerhöhen"}}
	
	local defenseSkill = skill(player, "defense")
	items[11] = {type="iron_chestplate", amount=defenseSkill, name="&b&2Verteidigung", lore={"&7Der Verteidigungs-Skill veringert den Schaden,", "&7der dir von Gegnern, egal mit welcher Waffe, zufügt wird.", "&6Dein aktueller Verteidigungs-Skill beträgt: &4" .. defenseSkill,
		"&6Effekt: &4-" .. (defenseSkill/2) .. " &6Schaden",
		"&7Linksklick um den Skill um &41 &7Level zuerhöhen (&4-0,5&7 Schaden)", "&7Rechtsklick um den Skill um &43 &7Level zuerhöhen (&4-1,5&7 Schaden)"}}
	
	if swordSkill == 0 then
			items[10].amount = 1
	end
	
	if defenseSkill == 0 then
			items[11].amount = 1
	end
	
	local inv = inventory.openChest(player, 5, "&b&5Skills", items)
	
	inv:on("click", function(event)
		event:setCancelled(true)
		
		if chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(event:getCurrentItem()), "§"), "&") == "Zurück" then
			if parentInv == nil then
				parentInv:close()
			else 
				parentInv:open(player)
			end
		elseif chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(event:getCurrentItem()), "§"), "&") == "Nahkampf" then
			if skillPoints > 0 then
				if event:getClick():toString() == "LEFT" then
					skillPoints = skillPoints - 1
					swordSkill = swordSkill + 1
					updateSkill(player, "sword", swordSkill)
				elseif event:getClick():toString() == "RIGHT" then
					if skillPoints >= 3 then
						skillPoints = skillPoints - 3
						swordSkill = swordSkill + 3
						updateSkill(player, "sword", swordSkill)
					else
						swordSkill = swordSkill + skillPoints
						skillPoints = 0
						updateSkill(player, "sword", swordSkill)
					end
				end
				storage.of(player).set("skillPoints", skillPoints)
				inv:close()
				openSkillInv(player)
			end
		elseif chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(event:getCurrentItem()), "§"), "&") == "Verteidigung" then
			if skillPoints > 0 then
				if event:getClick():toString() == "LEFT" then
					skillPoints = skillPoints - 1
					defenseSkill = defenseSkill + 1
					updateSkill(player, "defense", defenseSkill)
				elseif event:getClick():toString() == "RIGHT" then
					if skillPoints >= 3 then
						skillPoints = skillPoints - 3
						defenseSkill = defenseSkill + 3
						updateSkill(player, "defense", defenseSkill)
					else
						defenseSkill = defenseSkill + skillPoints
						skillPoints = 0
						updateSkill(player, "defense", defenseSkill)
					end
				end
				storage.of(player).set("skillPoints", skillPoints)
				inv:close()
				openSkillInv(player)
			end
		end
						
	end)
end

return {isPlayer = isPlayer, getTotalArmorDefensePoints = getTotalArmorDefensePoints, isNew = isNew, getHealth = getHealth, getMaxHealth = getMaxHealth, setDefaultStorage = setDefaultStorage, setHealth = setHealth, setMaxHealth = setMaxHealth, getExpAmountForLvl = getExpAmountForLvl, givePlayerXp = givePlayerXp, updateXpBar = updateXpBar, updateActionbar = updateActionbar, getLevel = getLevel,getXp = getXp, getTotalXp = getTotalXp, remainingSkillPoints = remainingSkillPoints, totalSkillPoints = totalSkillPoints, giveSkillPoints = giveSkillPoints, openSkillInv = openSkillInv, skill = skill, updateSkill = updateSkill}