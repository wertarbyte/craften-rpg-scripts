local rpgplus = require 'rpgplus'
local sound = require 'rpgplus.sound'
local storage = require 'rpgplus.storage'
local scheduler = require 'rpgplus.scheduler'

-- Überprüft, ob dem player gerade ein Song vorgespielt wird um dementsprechend ein neuen abzuspielen oder nicht.

local function song(player, song)
	if storage.of(player).get("activeSong") == false then
		local songpath = "songs/" .. song .. ".nbs"
		sound.playSong(player, songpath)
		storage.of(player).set("activeSong", true)
		local songdelay = 0
		-- min Pause zwischen Songs
		local pause = 30
		if song == "1" then
			songdelay = 20 * (156 + pause)
		elseif song == "2" then
			songdelay = 20 * (185 + pause)
		elseif song == "3" then
			songdelay = 20 * (185 + pause)
		elseif song == "4" then
			songdelay = 20 * (110 + pause)
		elseif song == "5" then
			songdelay = 20 * (103 + pause)
		elseif song == "6" then
			songdelay = 20 * (86 + pause)
		end
		scheduler.delay(songdelay, function()
			storage.of(player).set("activeSong", false)
		end)
	end
end

return song