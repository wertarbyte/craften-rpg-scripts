local system = luajava.bindClass("java.lang.System")

local function getSystemTime()
	return system:currentTimeMillis()
end

return {getSystemTime = getSystemTime}