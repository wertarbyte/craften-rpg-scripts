
local export = {}

export.distanceSquared = function(loc1, loc2)
	return (loc1.x - loc2.x)^2 + (loc1.y - loc2.y)^2 + (loc1.z - loc2.z)^2
end

export.distance = function(loc1, loc2)
	return math.sqrt(export.distanceSquared(loc1, loc2))
end

local function rotateAboutX(vector, pitch)
	local y = math.cos(pitch)*vector.y - math.sin(pitch)*vector.z
	local z = math.sin(pitch)*vector.y + math.cos(pitch)*vector.z
	vector.y = y
	vector.z = z
	
	return vector
end

local function rotateAboutY(vector, yaw)
	local x = math.cos(yaw)*vector.x + math.sin(yaw)*vector.z
	local z = -math.sin(yaw)*vector.x + math.cos(yaw)*vector.z
	vector.x = x
	vector.z = z
	
	return vector
end

local function rotateAboutZ(vector, roll)
	local x = math.cos(roll)*vector.x - math.sin(roll)*vector.y
	local y = math.sin(roll)*vector.x + math.cos(roll)*vector.y
	vector.x = x
	vector.y = y
	
	return vector
end

export.rotate = function(vector, pitch, yaw)
	local pitchR = pitch/180*math.pi
	local yawR = yaw/180*math.pi
	
	vector = rotateAboutX(vector, pitchR)
	vector = rotateAboutY(vector, -yawR)
	
	return vector
end	

return export