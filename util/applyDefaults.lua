local defaults = {}
setmetatable(defaults, {__mode = "k"})
local mt = {__index = function (t) return defaults[t] end}

return function (d)
    return function (t)
        for k,v in pairs(d) do
          if t[k] == nil then t[k] = v end
        end
    end
end
