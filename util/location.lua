local function convertJavaToLua(loc)
	local location = {world=loc:getWorld():getName(), x=loc:getX(), y=loc:getY(), z=loc:getZ()}
	return location
end

return {convertJavaToLua = convertJavaToLua}