local rpgplus = require 'rpgplus'
local inventory = require 'rpgplus.inventory'

local bronzecoin = require 'items/money/bronzecoin'
local emeraldcoin = require 'items/money/emeraldcoin'
local goldcoin = require 'items/money/goldcoin'
local silvercoin = require 'items/money/silvercoin'

local function giveAll(player, amount)
	for i=3,1,-1 do
		local coinValue=amount-(amount%64^i)
		local coinAmount=coinValue/64^i
		amount=amount-coinValue
		if coinAmount>=1 then
			if i==3 then
				inventory.giveItems(player, emeraldcoin(coinAmount))
			end
			if i==2 then
				inventory.giveItems(player, goldcoin(coinAmount))
			end
			if i==1 then
				inventory.giveItems(player, silvercoin(coinAmount))
			end
		end
	end
	if amount~=0 then
		inventory.giveItems(player, bronzecoin(amount))
	end
end

local function takeAll(player)
	local money = 0
	while inventory.hasItems(player, bronzecoin(1)) do
		inventory.takeItems(player, bronzecoin(1))
		money=money+1
	end
	while inventory.hasItems(player, silvercoin(1)) do
		inventory.takeItems(player, silvercoin(1))
		money=money+64
	end
	while inventory.hasItems(player, goldcoin(1)) do
		inventory.takeItems(player, goldcoin(1))
		money=money+64^2
	end
	while inventory.hasItems(player, emeraldcoin(1)) do
		inventory.takeItems(player, emeraldcoin(1))
		money=money+64^3
	end
	return money
end

local function give(player, amount)
	local money = takeAll(player)
	giveAll(player, money+amount)
end

local function take(player, amount)
	local money = takeAll(player)
	if money>tonumber(amount) then
		giveAll(player, money-amount)
		return true
	else
		giveAll(player, money)
		return false
	end
end

local function set(player, amount)
	local money = takeAll(player)
	giveAll(player, amount)
end

local function get(player)
	local money = takeAll(player)
	giveAll(player, money)
	return money
end

--"give(player, amount)" gives amount of money in the playerinventory. 
--"take(player, amount)" takes amount of money from the playerinventory. Returns false if the amount is higher as money in the playerinventory, otherwise true.
--"set(player, amount)" sets amount of money in the playerinventory.
--"get(player)" Returns amount of money in the playerinventory.

return {give=give, take=take, set=set, get=get}