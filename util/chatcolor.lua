local function removeColorCodes(text, colorCodeChar)
	
	if text == nil then
		return nil
	end
	
	local new = string.gsub(text, colorCodeChar .. "0", "")
	new = string.gsub(new, colorCodeChar .. "1", "")
	new = string.gsub(new, colorCodeChar .. "2", "")
	new = string.gsub(new, colorCodeChar .. "3", "")
	new = string.gsub(new, colorCodeChar .. "4", "")
	new = string.gsub(new, colorCodeChar .. "5", "")
	new = string.gsub(new, colorCodeChar .. "6", "")
	new = string.gsub(new, colorCodeChar .. "7", "")
	new = string.gsub(new, colorCodeChar .. "8", "")
	new = string.gsub(new, colorCodeChar .. "9", "")
	new = string.gsub(new, colorCodeChar .. "0", "")
	new = string.gsub(new, colorCodeChar .. "a", "")
	new = string.gsub(new, colorCodeChar .. "b", "")
	new = string.gsub(new, colorCodeChar .. "c", "")
	new = string.gsub(new, colorCodeChar .. "d", "")
	new = string.gsub(new, colorCodeChar .. "e", "")
	new = string.gsub(new, colorCodeChar .. "f", "")
	new = string.gsub(new, colorCodeChar .. "k", "")
	new = string.gsub(new, colorCodeChar .. "l", "")
	new = string.gsub(new, colorCodeChar .. "m", "")
	new = string.gsub(new, colorCodeChar .. "n", "")
	new = string.gsub(new, colorCodeChar .. "o", "")
	new = string.gsub(new, colorCodeChar .. "r", "")
	
	return new
end

local function translateColorCodes(text, colorCodeChar)
	
	if text == nil then
		return nil
	end
	
	local new = string.gsub(text, colorCodeChar .. "0", "")
	new = string.gsub(new, colorCodeChar .. "1", "§1")
	new = string.gsub(new, colorCodeChar .. "2", "§2")
	new = string.gsub(new, colorCodeChar .. "3", "§3")
	new = string.gsub(new, colorCodeChar .. "4", "§4")
	new = string.gsub(new, colorCodeChar .. "5", "§5")
	new = string.gsub(new, colorCodeChar .. "6", "§6")
	new = string.gsub(new, colorCodeChar .. "7", "§7")
	new = string.gsub(new, colorCodeChar .. "8", "§8")
	new = string.gsub(new, colorCodeChar .. "9", "§9")
	new = string.gsub(new, colorCodeChar .. "0", "§0")
	new = string.gsub(new, colorCodeChar .. "a", "§a")
	new = string.gsub(new, colorCodeChar .. "b", "§b")
	new = string.gsub(new, colorCodeChar .. "c", "§c")
	new = string.gsub(new, colorCodeChar .. "d", "§d")
	new = string.gsub(new, colorCodeChar .. "e", "§e")
	new = string.gsub(new, colorCodeChar .. "f", "§f")
	new = string.gsub(new, colorCodeChar .. "k", "§k")
	new = string.gsub(new, colorCodeChar .. "l", "§l")
	new = string.gsub(new, colorCodeChar .. "m", "§m")
	new = string.gsub(new, colorCodeChar .. "n", "§n")
	new = string.gsub(new, colorCodeChar .. "o", "§o")
	new = string.gsub(new, colorCodeChar .. "r", "§r")
	
	return new
end

return {removeColorCodes = removeColorCodes, translateColorCodes = translateColorCodes}