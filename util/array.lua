local export = {}

export.getRandomIndex = function(list)
	local listSize = 0
	for k,v in pairs(list) do
     listSize = listSize + 1
	end
	return list[math.random(1, listSize)]
end

export.size = function(list)
	local listSize = 0
	for k,v in pairs(list) do
     listSize = listSize + 1
	end
	
	return listSize
end

export.randomOf = function(...)
    local a = {...}
    return a[math.random(1, len(a))]
end

return export