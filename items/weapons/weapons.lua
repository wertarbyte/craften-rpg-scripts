local oakwoodsword = require 'items/weapons/oakwoodsword'
local birchwoodsword = require 'items/weapons/birchwoodsword'
local chatcolor = require 'util/chatcolor'


local weapons = {[oakwoodsword.getName()] = true, [birchwoodsword.getName()] = true}
local weaponsList = {[oakwoodsword.getName()] = oakwoodsword, [birchwoodsword.getName()] = birchwoodsword}

local function getStartWeapon()
	return birchwoodsword
end

local function isWeapon(itemDisplayName)
	local itemname = chatcolor.removeColorCodes(itemDisplayName, "&")
	itemname = chatcolor.removeColorCodes(itemname, "§")
	
	if weapons[itemname] then
		return true
	end
	
	return false
end

local function getWeapon(itemDisplayName)
	if isWeapon(itemDisplayName) then
		local itemname = chatcolor.removeColorCodes(itemDisplayName, "&")
		itemname = chatcolor.removeColorCodes(itemname, "§")
		return weaponsList[itemname]
	end
	return nil
end

return {getStartWeapon = getStartWeapon, isWeapon = isWeapon, getWeapon = getWeapon}