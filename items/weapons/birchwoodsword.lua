local mindamage = 4
local maxdamage = 6
local name = "Birkenholzschwert"
local displayname = "&r" .. name

local function get()
	return {type="wood_sword", amount=1, name=displayname, lore={" ", "&6Schaden: &4" .. mindamage .. "-" .. maxdamage," ", " ", "&cNicht verkaufbar!", "&fStandart Item"},unbreakable=true}
end

local function getDamage()
	return math.random(mindamage, maxdamage)
end

local function getName()
	return name
end

local function getDisplayName()
	return displayname
end

return {get = get, getDamage = getDamage, getName = getName, getDisplayName = getDisplayName}
