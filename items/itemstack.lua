local chatcolor = require 'util/chatcolor'

local function getDisplayName(javaItemStack)
	if javaItemStack == nil then
		return nil
	else
		local itemmeta = javaItemStack:getItemMeta()
		
		if itemmeta == nil then
			return nil
		end
		
		if itemmeta:hasDisplayName() then
			return itemmeta:getDisplayName()
		end
		return javaItemStack:getType():toString()
	end
end

local function convertLuaToJava(luaStack)
	local material = luajava.bindClass("org.bukkit.Material"):valueOf(string.upper(luaStack.type))
	local itemStack = luajava.newInstance("org.bukkit.inventory.ItemStack", material, luaStack.amount)
	local itemMeta = itemStack:getItemMeta()
	itemMeta:setDisplayName(chatcolor.translateColorCodes(luaStack.name, "&"))
	
	itemStack:setItemMeta(itemMeta)
	
	return itemStack
end

local function getPlainName(item)
	return chatcolor.removeColorCodes(chatcolor.removeColorCodes(getDisplayName(item), "§"), "&")
end

return {getDisplayName = getDisplayName,convertLuaToJava = convertLuaToJava, getPlainName = getPlainName}