local name = "Lederhose"
local displayname = "&r" .. name

local defense = 3

local function get()
	return {type="leather_leggings", amount=1, name=displayname, lore={" ", "&6Schutz: &4" .. defense, " ", " ", "&rStandart Item"},unbreakable=true}
end

local function getName()
	return name
end

local function getDisplayName()
	return displayname
end

local function getDefense()
	return defense
end

return {get = get, getName = getName, getDisplayName = getDisplayName, getDefense = getDefense}