local name = "Lederjacke"
local displayname = "&r" .. name

local defense = 4

local function get()
	return {type="leather_chestplate", amount=1, name=displayname, lore={" ", "&6Schutz: &4" .. defense, " ", " ", "&rStandart Item"},unbreakable=true}
end

local function getName()
	return name
end

local function getDisplayName()
	return displayname
end

local function getDefense()
	return defense
end

return {get = get, getName = getName, getDisplayName = getDisplayName, getDefense = getDefense}