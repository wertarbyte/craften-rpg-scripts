local chatcolor = require 'util/chatcolor'
local leathercap = require 'items/armor/leathercap'
local leatherjacket = require 'items/armor/leatherjacket'
local leatherpants = require 'items/armor/leatherpants'
local leatherboots = require 'items/armor/leatherboots'

local armorList = {[leathercap.getName()] = leathercap, 
					[leatherjacket.getName()] = leatherjacket, 
					[leatherpants.getName()] = leatherpants,
					[leatherboots.getName()] = leatherboots}

local function isArmor(itemDisplayName)
	local itemname = chatcolor.removeColorCodes(itemDisplayName, "&")
	itemname = chatcolor.removeColorCodes(itemname, "§")
	
	if armorList[itemname] == nil then
		return false
	else
		return true
	end
	
end

local function getArmor(itemDisplayName)
	if isArmor(itemDisplayName) then
		local itemname = chatcolor.removeColorCodes(itemDisplayName, "&")
		itemname = chatcolor.removeColorCodes(itemname, "§")
		return armorList[itemname]
	end
end

return {isArmor = isArmor, getArmor = getArmor}