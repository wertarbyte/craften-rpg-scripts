local rpgplus = require 'rpgplus'

rpgplus.on("weather.change", function(event)
	if event:toWeatherState() then
		event:setCancelled(true)
	end
end)