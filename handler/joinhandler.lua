local rpgplus = require 'rpgplus'
local storage = require 'rpgplus.storage'
local playerUtil = require 'util/player'
local system = require 'util/system'
local characterinfo = require 'items/characterinfo'
local itemstack = require 'items/itemstack'

rpgplus.on("player.join", function(event)
	if playerUtil.isNew(event:getPlayer()) then
		playerUtil.setDefaultStorage(event:getPlayer())
		local playerInv = event:getPlayer():getInventory()
		playerInv:setItem(8, itemstack.convertLuaToJava(characterinfo()))
	end
	playerUtil.updateXpBar(event:getPlayer())
	playerUtil.updateActionbar(event:getPlayer())
end)
