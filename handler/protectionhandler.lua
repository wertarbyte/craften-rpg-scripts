local rpgplus = require 'rpgplus'

rpgplus.on("player.interact", function(event)
	if not event:getPlayer():hasPermission("rpgplus.build") then
		event:setCancelled(true)
	end
end)

rpgplus.on("player.interact", function(event)
	if event:getAction():toString() == "PHYSICAL" and event:getClickedBlock():getType():toString() == "SOIL" then
		event:setCancelled(true)
	end
end)

rpgplus.on("block.break", function(event)
	if not event:getPlayer():hasPermission("rpgplus.build") then
		event:setCancelled(true)
	end
end)
rpgplus.on("block.place", function(event)
	if not event:getPlayer():hasPermission("rpgplus.build") then
		event:setCancelled(true)
	end
end)