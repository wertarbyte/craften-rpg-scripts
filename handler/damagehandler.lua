local rpgplus = require 'rpgplus'

local playerUtil = require 'util/player'

rpgplus.on("entity.damage", function(event)
	if playerUtil.isPlayer(event:getEntity()) then
		
		if event:getCause():toString() == "FALL" then
			
			local oldPlayerHealth = playerUtil.getHealth(event:getEntity())
			local newPlayerHealth = oldPlayerHealth - event:getDamage()
			playerUtil.setHealth(event:getEntity(), newPlayerHealth)
			event:setDamage(0)
		end
	end
end)