local rpgplus = require 'rpgplus'

local playerUtil = require 'util/player'

rpgplus.on("entity.health.regain", function(event)
	if playerUtil.isPlayer(event:getEntity()) then
		event:setCancelled(true)
		local oldPlayerHealth = playerUtil.getHealth(event:getEntity())
		local newPlayerHealth = oldPlayerHealth + event:getAmount()
		
		if newPlayerHealth > playerUtil.getMaxHealth(event:getEntity()) then
			newPlayerHealth = playerUtil.getMaxHealth(event:getEntity())
		end
		
		playerUtil.setHealth(event:getEntity(), newPlayerHealth)
	end
end)
