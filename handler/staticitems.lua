local rpgplus = require 'rpgplus'
local itemstack = require 'items/itemstack'
local chatcolor = require 'util/chatcolor'
local export = {}

export.newStaticItem = function (itemName)
	rpgplus.on("inventory.click", function(event)
		local item = event:getCurrentItem()
		if chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(item), "§"), "&") == itemName then
			if not event:getInventory():getHolder():hasPermission("rpgplus.staticitem.bypass") then
				event:setCancelled(true)
			end		
		end
	end)
	
	rpgplus.on("player.item.drop", function(event)
		local item = event:getItemDrop():getItemStack()
		if chatcolor.removeColorCodes(chatcolor.removeColorCodes(itemstack.getDisplayName(item), "§"), "&") == itemName then
			if not event:getPlayer():hasPermission("rpgplus.staticitem.bypass") then
				event:setCancelled(true)
			end
		end
	end)
end

return export;